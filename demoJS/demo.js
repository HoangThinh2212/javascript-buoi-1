// để chạy console.log cho biến bất kỳ, chọn biến đó và ấn tổ hợp Ctrl shift L


console.log("Hello Tony");
var username = "Tony";

console.log(username);

var age = 2; //type number

var isMarried = true; //true false type boolean

var isLogin = true;

isMarried = false; //update value

// 2 type to name variable: Camel case AND Snake case ---- isMarried (Camel) ----- is_married (Snake) ---- 2 type is 2 different var (not same not use together)

//null and undefined
// null --- could be assigned
// undefined --- never assigned
var hasChildren = undefined;

// operators Toán tử
var num1 = 2;
var num2 = 5;
var num3 = num1 + num2; // operator: + - * / %
console.log("num3:", num3);

// Toán tử dồn: += -= *= /=

// ++ --
var num8 = 5;
num8++;
// biến++ là tính toán trước rồi mới tăng giá trị biến
// ++biến là tăng giá trị biến trước rồi mới tính toán
console.log("num8:", num8);

num9 = num8 + 2;


