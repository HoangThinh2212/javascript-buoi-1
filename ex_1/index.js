/*
Exercise 1
Input:
Số ngày đi làm

Step hoặc các bước xử lý:
Step 1: nhập số ngày đi làm
Step 2: tạo biến chứa tiền lương 1 ngày
Step 3: tạo biến chứa số ngày đi làm được nhập vào
Step 4: Tạo biến chứa tiền của số ngày đi làm với công thức tính như sau:
Lương số ngày đi làm = lương 1 ngày * số ngày đi làm
Step 5: console.log để xuất màn hình

Đầu ra hoặc Output:
Lương số ngày đi làm

*/

var workday;
workday = 3;
const salary = 100000;
var finalSalary;

finalSalary = salary * workday;
console.log("Lương số ngày đi làm: ", finalSalary.toLocaleString());
// finalSalary sẽ là 300000
/* 
Exercise 2

Input:
Nhập vào 5 số thực

Step:
Step 1: tạo ra 5 biến chứa 5 giá trị là số thực và 1 biến chứa giá trị trung bình cộng của 5 số đó
Step 2: Nhập vào 5 số thực mong muốn
Step 3: tính trung bình cộng của 5 số vừa được nhập
Step 4: console.log để xuất màn hình

Output:
Giá trị của trung bình cộng 5 số đó

*/

var a = 1.4,
  b = 1.6,
  c = 1.5,
  d = 1.5,
  e = 0;
var result;

result = (a + b + c + d + e) / 5;
console.log("Trung bình cộng của 5 số: ", result);
//Kết quả result sẽ là 1.2

/*
Exercise 3

Input: 
Giá tiền USD

Step:
Step 1: Tạo ra biến chứa giá tiền USD người dùng muốn đổi
Step 2: người dùng nhập vào giá tiền USD muốn đổi
Step 3: tính số tiền vnd được đổi từ số tiền USD vừa nhập
Step 4: console.log để xuất màn hình

Output:
Giá tiền VND

*/
var usdAmount;
usdAmount = 4;
// người dùng nhập số tiền USD muốn quy đổi
const usdValue = 23500;
var vndExchange;

vndExchange = usdAmount * usdValue;
console.log("Giá tiền theo VNĐ : ", vndExchange.toLocaleString());

/*
Exercise 4:

Input:
Chiều dài và chiều rộng của hcn


Step: 
Step 1: Tạo ra 2 biến lưu trữ chiều dài và chiều rộng của hình chữ nhật và 2 biến khác lưu trữ giá trị kết quả của phép tính chu vi và diện tích của hình chữ nhật đó
Step 2: Nhập chiều dài và chiều rộng của hcn đó
Step 3: Tính chu vi và diện tích của hình chữ nhật đó
Step 4: console.log để xuất màn hình


Output:
Kết quả của phép tính chu vi và diện tích hình chữ nhật

*/
var chieu_dai, chieu_rong;
chieu_dai = 2;
chieu_rong = 6;
var chu_vi, dien_tich;

dien_tich = chieu_dai * chieu_rong;
chu_vi = (chieu_dai + chieu_rong) * 2;

console.log("Diện tích: ", dien_tich);
console.log("Chu vi: ", chu_vi);

/*
Exercise 5

Input:
1 Số có 2 chữ số

Step:
Step 1: Tạo ra 1 biến lưu trữ 1 số có 2 chữ số
Step 2: Lấy số hàng đơn vị từ số vừa nhập
Step 3: Lấy số hàng chục từ số vừa nhập
Step 4: Tính tổng của 2 ký số vừa nhập
Step 5: console.log để xuất màn hình

Output: 
Giá trị tổng của 2 ký số vừa nhập

*/

var so_nhap, hang_don_vi, hang_chuc, tong_ky_so;
so_nhap = 42;
hang_don_vi = so_nhap % 10;
hang_chuc = so_nhap / 10;
tong_ky_so = hang_chuc + hang_don_vi;
// dùng hàm Floor để làm tròn về số gần nhất
var ketqua = Math.floor(tong_ky_so);
console.log("Tổng ký số là: ", ketqua);
